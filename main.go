package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

//Jar is struct to store cookie
type Jar struct {
	lk      sync.Mutex
	cookies map[string][]*http.Cookie
}

//NewJar is func to create new Jar
func NewJar() *Jar {
	jar := new(Jar)
	jar.cookies = make(map[string][]*http.Cookie)
	return jar
}

//SetCookies is func to set cookie to Jar
func (jar *Jar) SetCookies(u *url.URL, cookies []*http.Cookie) {
	jar.lk.Lock()
	jar.cookies[u.Host] = cookies
	jar.lk.Unlock()
}

//SetCookies is func to set cookie to Jar
func (jar *Jar) SetCookiesString(u string, cookies []*http.Cookie) {
	jar.lk.Lock()
	jar.cookies[u] = cookies
	jar.lk.Unlock()
}

//Cookies is func to return cookie by host
func (jar *Jar) Cookies(u *url.URL) []*http.Cookie {
	return jar.cookies[u.Host]
}

//Cookies is func to return cookie by string
func (jar *Jar) CookiesByString(u string) []*http.Cookie {
	return jar.cookies[u]
}

//struct to store all Handles and data
type myHandler struct {
	mux         map[string]func(http.ResponseWriter, *http.Request, *RAMSettings)
	ramSettings RAMSettings
}

func (m *myHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if h, ok := m.mux[r.URL.Path]; ok {
		h(w, r, &m.ramSettings)
		return
	}
	http.ServeFile(w, r, "bot"+r.URL.Path)
}

func main() {
	//init logfile
	f, err := os.OpenFile("logfile", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)

	//init database
	db, err := sql.Open("sqlite3", "./data.db")
	if err != nil {
		fmt.Println(err)
	}
	defer db.Close()

	//init router
	m := &myHandler{}
	m.ramSettings = RAMSettings{}
	m.ramSettings.db = db
	m.ramSettings.refreshAccountsSycn = make(chan Account)
	m.ramSettings.refreshAccountsExit = make(chan struct{})

	initSettingsTable(db)
	initUsersTable(db)
	initHandlers(m)
	initSettingsRAM(&m.ramSettings)
	initAuthThreads(&m.ramSettings)

	//init server
	s := &http.Server{
		Addr:         ":3590",
		Handler:      m,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	log.Fatal(s.ListenAndServe())
}

func initHandlers(m *myHandler) {
	m.mux = make(map[string]func(http.ResponseWriter, *http.Request, *RAMSettings))
	m.mux["/get_settings"] = getSettingsHandler
	m.mux["/set_settings"] = setSettingsHandler
	m.mux["/add_accounts"] = addAccountsHandler
	m.mux["/get_accounts"] = getAccountsHandler
	m.mux["/del_accounts"] = deleteAccounts
	m.mux["/chk_accounts"] = checkAccountsNow
}

func initAuthThreads(rs *RAMSettings) {
	for i := int64(0); i < rs.Threads; i++ {
		go refreshAccounts(rs.refreshAccountsSycn, rs.refreshAccountsExit, rs.db)
	}
	go syncRefreshAccount(rs)
}

func initSettingsRAM(rs *RAMSettings) {
	//get all settings from db
	rows, err := rs.db.Query("SELECT name, value FROM settings")
	checkErr(err)

	//Vars for Scan from setting table
	var value int64
	var name string

	//append values from rows to settings
	for rows.Next() {
		//Scan from row to vars
		err = rows.Scan(&name, &value)
		checkErr(err)

		switch name {
		case "threads":
			rs.Threads = value
		case "try_to_auth":
			rs.TryToAuth = value
		case "interval_check_accs":
			rs.Interval = value
		case "cost_1v":
			rs.Cost1v = value
		case "mul_1v":
			rs.Mul1v = value
		case "cost_1l":
			rs.Cost1l = value
		case "mul_1l":
			rs.Mul1l = value
		case "cost_1s":
			rs.Cost1s = value
		case "mul_1s":
			rs.Mul1s = value
		case "cost_1c":
			rs.Cost1c = value
		case "mul_1c":
			rs.Mul1c = value
		}
	}
	//important!
	rows.Close()
}

//Create table users
func initUsersTable(db *sql.DB) {
	stmt, err := db.Prepare("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, password TEXT, money INTEGER, last_check INTEGER, proxy TEXT, cookie TEXT);")
	checkErr(err)

	_, err = stmt.Exec()
	checkErr(err)
}

//Create table settings
func initSettingsTable(db *sql.DB) {
	_, err := db.Query("SELECT * FROM settings")
	if err != nil && err.Error() == "no such table: settings" {
		stmt, err := db.Prepare("CREATE TABLE IF NOT EXISTS settings (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, value INTEGER);")
		checkErr(err)

		_, err = stmt.Exec()
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "threads", 1)
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "try_to_auth", 1)
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "interval_check_accs", 360)
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "cost_1v", 1)
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "mul_1v", 1)
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "cost_1l", 1)
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "mul_1l", 1)
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "cost_1s", 1)
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "mul_1s", 1)
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "cost_1c", 1)
		checkErr(err)

		_, err = db.Exec("INSERT INTO settings (name, value) values(?,?)", "mul_1c", 1)
		checkErr(err)
	} else {
		checkErr(err)
	}
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
