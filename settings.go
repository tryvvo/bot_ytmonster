package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync/atomic"
)

//RAMSettings is struct is stored in RAM
type RAMSettings struct {
	Threads   int64
	TryToAuth int64
	Interval  int64
	Cost1v    int64
	Mul1v     int64
	Cost1l    int64
	Mul1l     int64
	Cost1s    int64
	Mul1s     int64
	Cost1c    int64
	Mul1c     int64

	db                  *sql.DB
	refreshAccountsSycn chan Account
	refreshAccountsExit chan struct{}
}

//Setting is Standart struct for Setting
type Setting struct {
	Name  string `json:"name"`
	Value int64  `json:"val"`
}

//Settings  is struct for array of Setting
type Settings struct {
	Settings []Setting `json:"settings"`
}

//Return json array of all Settings
func getSettingsHandler(w http.ResponseWriter, r *http.Request, rs *RAMSettings) {
	//get all settings to rows
	rows, err := rs.db.Query("SELECT name, value FROM settings")
	checkErr(err)

	//Vars for Scan from setting table
	var value int64
	var name string

	//output array
	var settings Settings

	//append values from rows to settings
	for rows.Next() {
		//Scan from row to vars
		err = rows.Scan(&name, &value)
		checkErr(err)

		//append to settings
		settings.Settings = append(settings.Settings, Setting{name, value})
	}
	//important!
	rows.Close()

	//Try to marshal array of settings
	if res, err := settings.MarshalJSON(); err == nil {
		fmt.Fprint(w, string(res))
	} else {
		fmt.Fprint(w, `{"error":50}`)
		checkErr(err)
	}
}

//get JSON Array of Setting
//print status
func setSettingsHandler(w http.ResponseWriter, r *http.Request, rs *RAMSettings) {
	//Var to store unmarshal JSON Array
	var settings Settings

	// read from request to var
	body, err := ioutil.ReadAll(r.Body)
	checkErr(err)

	//unmarshal to accounts
	err = settings.UnmarshalJSON(body)
	if err != nil {
		fmt.Println(err)
		fmt.Fprint(w, `{"error":10}`)
		return
	}

	//Iterate array of setting
	for _, i := range settings.Settings {
		//Select right setting
		switch i.Name {
		case "threads":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='threads'`, i.Value)
			checkErr(err)

			//refresh Threads
			go func(i int64, rs *RAMSettings) {
				//Close all old refreshAccounts threads
				for j := int64(0); j < atomic.LoadInt64(&rs.Threads); i++ {
					rs.refreshAccountsExit <- struct{}{}
				}

				//upgrade RAMSettings Thread
				atomic.StoreInt64(&rs.Threads, i)

				//Run new refreshAccounts threads
				for j := int64(0); j < i; j++ {
					go refreshAccounts(rs.refreshAccountsSycn, rs.refreshAccountsExit, rs.db)
				}
			}(i.Value, rs)

		case "try_to_auth":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='try_to_auth'`, i.Value)
			checkErr(err)
			atomic.StoreInt64(&rs.TryToAuth, i.Value)
		case "interval_check_accs":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='interval_check_accs'`, i.Value)
			checkErr(err)
			atomic.StoreInt64(&rs.Interval, i.Value)
		case "cost_1v":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='cost_1v'`, i.Value)
			checkErr(err)
			atomic.StoreInt64(&rs.Cost1v, i.Value)
		case "mul_1v":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='mul_1v'`, i.Value)
			checkErr(err)
			atomic.StoreInt64(&rs.Mul1v, i.Value)
		case "cost_1l":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='cost_1l'`, i.Value)
			checkErr(err)
			atomic.StoreInt64(&rs.Cost1l, i.Value)
		case "mul_1l":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='mul_1l'`, i.Value)
			checkErr(err)
			atomic.StoreInt64(&rs.Mul1l, i.Value)
		case "cost_1s":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='cost_1s'`, i.Value)
			checkErr(err)
			atomic.StoreInt64(&rs.Cost1s, i.Value)
		case "mul_1s":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='mul_1s'`, i.Value)
			checkErr(err)
			atomic.StoreInt64(&rs.Mul1s, i.Value)
		case "cost_1c":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='cost_1c'`, i.Value)
			checkErr(err)
			atomic.StoreInt64(&rs.Cost1c, i.Value)
		case "mul_1c":
			_, err = rs.db.Exec(`UPDATE settings SET value=? WHERE "name"='mul_1c'`, i.Value)
			checkErr(err)
			atomic.StoreInt64(&rs.Mul1c, i.Value)
		}
	}
	fmt.Fprint(w, `{"status":"OK"}`)
}
