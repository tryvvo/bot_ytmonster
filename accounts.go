package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync/atomic"
	"time"
)

//Account is main class contains data
type Account struct {
	Email     string         `json:"email"`
	Password  string         `json:"password"`
	Proxy     string         `json:"proxy"`
	Money     int64          `json:"money"`
	LastCheck int64          `json:"last_check"`
	Cookies   []*http.Cookie `json:"-"`
}

//Accounts is main class to store array of Account
type Accounts struct {
	Array []Account `json:"accounts"`
}

func deleteAccounts(w http.ResponseWriter, r *http.Request, rs *RAMSettings) {
	//Var to store unmarshal JSON Array
	var accounts Accounts

	// read from request to var
	body, err := ioutil.ReadAll(r.Body)
	checkErr(err)

	//unmarshal to accounts
	err = accounts.UnmarshalJSON(body)
	if err != nil {
		fmt.Fprint(w, `{"error":10}`)
		return
	}

	//Delete from bd
	for _, i := range accounts.Array {
		rs.db.Exec(`DELETE FROM users WHERE "email"=?`, i.Email)
	}
	fmt.Fprint(w, `{"status":"OK"}`)
}

func checkAccountsNow(w http.ResponseWriter, r *http.Request, rs *RAMSettings) {
	//Var to store unmarshal JSON Array
	var accounts Accounts

	// read from request to var
	body, err := ioutil.ReadAll(r.Body)
	checkErr(err)

	//unmarshal to accounts
	err = accounts.UnmarshalJSON(body)
	if err != nil {
		fmt.Fprint(w, `{"error":10}`)
		return
	}

	var email string
	var password string
	var proxy string
	var cookie string
	//Delete from bd
	for _, i := range accounts.Array {
		err := rs.db.QueryRow("SELECT email, password, proxy, cookie FROM users WHERE email=?", i.Email).Scan(&email, &password, &proxy, &cookie)
		if err == nil {
			cookies := []*http.Cookie{}
			if cookie != "" {
				err = json.Unmarshal([]byte(cookie), &cookies)
				checkErr(err)
			} else {
				cookies = nil
			}
			go func() {
				rs.refreshAccountsSycn <- Account{Email: email, Password: password, Proxy: proxy, Cookies: cookies}
			}()
		}
	}
	fmt.Fprint(w, `{"status":"OK"}`)
}

//print array of users
func getAccountsHandler(w http.ResponseWriter, r *http.Request, rs *RAMSettings) {
	//Var to store Array of Account
	var accounts Accounts
	accounts.Array = []Account{}

	//get all users to rows
	rows, err := rs.db.Query("SELECT email, password, money, last_check, proxy FROM users")
	checkErr(err)

	//Vars for Scan from setting table
	var money int64
	var lastCheck int64
	var email string
	var password string
	var proxy string

	//append values from rows to accounts
	for rows.Next() {
		//Scan from row to vars
		err = rows.Scan(&email, &password, &money, &lastCheck, &proxy)
		checkErr(err)

		//append to accounts
		accounts.Array = append(accounts.Array, Account{email, password, proxy, money, lastCheck, nil})
	}
	//important!
	rows.Close()

	//Try to marshal array of account
	if res, err := accounts.MarshalJSON(); err == nil {
		fmt.Fprint(w, `{"max_attempt":`+strconv.FormatInt(atomic.LoadInt64(&rs.TryToAuth), 10)+`,`+string(res)[1:])
	} else {
		fmt.Fprint(w, `{"error":50}`)
		checkErr(err)
	}
}

//get JSON Array of Account
//print status
func addAccountsHandler(w http.ResponseWriter, r *http.Request, rs *RAMSettings) {
	//Var to store unmarshal JSON Array
	var accounts Accounts

	// read from request to var
	body, err := ioutil.ReadAll(r.Body)
	checkErr(err)

	//unmarshal to accounts
	err = accounts.UnmarshalJSON(body)
	if err != nil {
		fmt.Fprint(w, `{"error":10}`)
		return
	}

	//Update if exist else insert
	for _, i := range accounts.Array {
		var id int64
		//try to select by email
		err := rs.db.QueryRow("SELECT id FROM users WHERE email=?", i.Email).Scan(&id)

		//check error
		if err == nil {
			_, err = rs.db.Exec(`UPDATE users SET "password"=?, "money"=?, "last_check"=?, "proxy"=? WHERE "email"=?`, i.Password, 0, 0, i.Proxy, i.Email)
			checkErr(err)
			go func() {
				rs.refreshAccountsSycn <- Account{Email: i.Email, Password: i.Password, Proxy: i.Proxy, Cookies: nil}
			}()
		} else {
			if err == sql.ErrNoRows {
				_, err = rs.db.Exec("INSERT INTO users (email, password, money, last_check, proxy, cookie) values(?,?,?,?,?,?)", i.Email, i.Password, 0, 0, i.Proxy, "")
				checkErr(err)
				go func() {
					rs.refreshAccountsSycn <- Account{Email: i.Email, Password: i.Password, Proxy: i.Proxy, Cookies: nil}
				}()
			} else {
				//if the error is related to server
				fmt.Fprint(w, `{"error":100}`)
				checkErr(err)
			}
		}
	}
	fmt.Fprint(w, `{"status":"OK"}`)
}

//func to authAccount by login and password return Cookie if good response, return nil if bad
func authAccount(email, password, proxy string, db *sql.DB, cookies []*http.Cookie) []*http.Cookie {
	//Create new Cookie struct
	jar := NewJar()

	if cookies != nil {
		jar.SetCookiesString("www.ytmonster.net", cookies)
	}
	//Client to do requests
	var client http.Client

	//Set proxy if exists
	if proxy != "" {
		proxyURL, err := url.Parse("http://" + proxy)
		checkErr(err)
		client = http.Client{
			Jar:       jar,
			Transport: &http.Transport{Proxy: http.ProxyURL(proxyURL)},
		}
	} else {
		client = http.Client{
			Jar: jar,
		}
	}

	//check cookie
	if cookies != nil {
		resp, err := client.Get("https://www.ytmonster.net/dashboard")
		checkErr(err)
		b, err := ioutil.ReadAll(resp.Body)
		checkErr(err)
		resp.Body.Close()

		body := string(b)

		if strings.Contains(body, "<title>YTMonster | Dashboard</title>") {
			if strings.Contains(body, "Redeem") {
				client.Get("https://www.ytmonster.net/dashboard?reedem=views")
			}

			//Try to marshal cookie
			jsonBytes, err := json.Marshal(client.Jar.(*Jar).CookiesByString("www.ytmonster.net"))
			jsonCookie := string(jsonBytes)

			//Get account balanse and refresh check time
			slices := strings.Split(strings.Split(strings.Split(body, "db-lgText db")[1], "\">")[0], "\"")
			_, err = db.Exec(`UPDATE users SET "last_check"=?, "money"=?, "cookie"=? WHERE "email"=?`, time.Now().Unix(), strings.Replace(slices[len(slices)-1], ".", "", -1), jsonCookie, email)
			checkErr(err)
			return client.Jar.(*Jar).CookiesByString("www.ytmonster.net")
		}
	}
	//First request to emulate
	resp, err := client.Get("https://www.ytmonster.net")
	checkErr(err)

	b, err := ioutil.ReadAll(resp.Body)
	checkErr(err)
	resp.Body.Close()

	//Check internet and proxy
	if !strings.Contains(string(b), "<title>YTMonster | Free YouTube Views, Likes, Subscribers, Comments</title>") {
		return nil
	}

	//emulate serf the site
	_, err = client.Get("https://www.ytmonster.net/login")
	checkErr(err)

	//set post data
	PostData := strings.NewReader("usernames=" + email + "&passwords=" + password)

	//generate post request
	req, err := http.NewRequest("POST", "https://www.ytmonster.net/login?login=ok", PostData)
	checkErr(err)

	req.Header.Set("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
	req.Header.Set("accept-encoding", "gzip, deflate, br")
	req.Header.Set("accept-language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7")
	req.Header.Set("cache-control", "max-age=0")
	req.Header.Set("content-length", "35")
	req.Header.Set("content-type", "application/x-www-form-urlencoded")
	req.Header.Set("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36")

	//try to login
	_, err = client.Do(req)
	checkErr(err)

	//try to get to dashboard
	resp, err = client.Get("https://www.ytmonster.net/dashboard")
	checkErr(err)
	b, err = ioutil.ReadAll(resp.Body)
	checkErr(err)
	resp.Body.Close()

	body := string(b)

	var retCookie []*http.Cookie
	retCookie = nil
	//Check if login was success
	if strings.Contains(body, "<title>YTMonster | Dashboard</title>") {
		if strings.Contains(body, "Redeem") {
			client.Get("https://www.ytmonster.net/dashboard?reedem=views")
		}

		//Try to marshal cookie
		jsonBytes, err := json.Marshal(client.Jar.(*Jar).CookiesByString("www.ytmonster.net"))
		jsonCookie := string(jsonBytes)

		//Get account balanse and refresh check time
		slices := strings.Split(strings.Split(strings.Split(body, "db-lgText db")[1], "\">")[0], "\"")
		_, err = db.Exec(`UPDATE users SET "last_check"=?, "money"=?, "cookie"=? WHERE "email"=?`, time.Now().Unix(), strings.Replace(slices[len(slices)-1], ".", "", -1), jsonCookie, email)
		checkErr(err)
		retCookie = client.Jar.(*Jar).CookiesByString("www.ytmonster.net")
	} else {
		_, err = db.Exec(`UPDATE users SET "last_check"="last_check"-1 WHERE "email"=?`, email)
	}

	//return cookie by url
	return retCookie
}

//func to check Tasks that bd contains
func checkTasks(cookies []*http.Cookie) {
	//TODO check tasks
}

//Thread to check accounts
func refreshAccounts(channel <-chan Account, exit <-chan struct{}, db *sql.DB) {
	for {
		select {
		case <-exit:
			return
		case acc := <-channel:
			//Try to auth
			cookies := authAccount(acc.Email, acc.Password, acc.Proxy, db, acc.Cookies)

			if cookies != nil {
				checkTasks(cookies)
			}
		}
	}
}

//Sync Thread to check accounts
func syncRefreshAccount(rs *RAMSettings) {
	t := time.NewTimer(0)
	for {
		<-t.C
		minTime := int64(-1)

		//get all users to rows
		rows, err := rs.db.Query("SELECT email, password, proxy, last_check, cookie FROM users")
		checkErr(err)

		//Vars for Scan from setting table
		var lastCheck int64
		var email string
		var password string
		var proxy string
		var cookie string

		//append values from rows to accounts
		for rows.Next() {
			//Scan from row to vars
			err = rows.Scan(&email, &password, &proxy, &lastCheck, &cookie)
			checkErr(err)

			cookies := []*http.Cookie{}
			if cookie != "" {
				err = json.Unmarshal([]byte(cookie), &cookies)
				checkErr(err)
			} else {
				cookies = nil
			}

			//Check that account is unchecked or valid
			if lastCheck > -atomic.LoadInt64(&rs.TryToAuth) {
				//Is the account need to check
				if time.Now().Unix()-lastCheck >= atomic.LoadInt64(&rs.Interval) || lastCheck <= 0 {
					//Add to channel Account
					go func() {
						rs.refreshAccountsSycn <- Account{Email: email, Password: password, Proxy: proxy, Cookies: cookies}
					}()
				} else {
					//get smallest time
					if minTime > atomic.LoadInt64(&rs.Interval)-time.Now().Unix()+lastCheck || minTime == -1 {
						minTime = atomic.LoadInt64(&rs.Interval) - time.Now().Unix() + lastCheck
						if minTime < 0 {
							minTime = 0
						}
					}
				}
			}
		}
		//important!
		rows.Close()

		if minTime != -1 {
			t.Reset(time.Second * time.Duration(minTime+5))
		} else {
			t.Reset(time.Second * time.Duration(atomic.LoadInt64(&rs.Interval)))
		}
	}
}
